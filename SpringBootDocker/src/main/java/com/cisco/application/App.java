package com.cisco.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication(scanBasePackages={"com.cisco.controller","com.cisco.application"})
public class App {

	//private static final Logger log = LoggerFactory.getLogger(App.class);
	@RequestMapping(value="/") 
	public String home(){
		return "hi , u are good !!";
    	  
      }
	  public static void main(String[] args) {
	    SpringApplication.run(App.class, args);
	}
	  
}
